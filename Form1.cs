﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

/*
 * Таблица "Количество участков"
 * part_count - один единственный столбец, одна единственная строка, содержащая количество таблиц-участков
 * или же добавить столбец с номером участка.
 * 
 * А неверно еще лучше добавлять таблицу с заранее созданными 36 строк пустых. и заполнять их...
 * 
 * 
 * 
 * 
 * 
 * 
 */



namespace telephone_allocation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /*
         * Потом просто подключить класс, который на входе берет
         * таблицу, и выводит ее на печать по заранее отработанному шаблону
         * 
         * И да - при добавлении автоматически установка указателя и прокрутка
         * таблицы таким образом, что всегда видно последние добавленные данные...
         * так что не мудрить.
         * Единственно попробовать сделать ввод не в таблицу непосредственно,
         * а в текстовые поля перед или внизу таблицы.
         */

        //*************************************************************************

        private void timerStart()
        {
            if ( (fileNameDB != "") && (!saved) )
            {
                labelAutoSaveState.Text = "ВКЛ";
                labelAutoSaveState.BackColor = Color.LightGreen;
                timer1.Start();
                //timer2.Start();
            }
        }

        private void timerStop()
        {
            labelAutoSaveState.Text = "ОТКЛ";
            labelAutoSaveState.BackColor = DefaultBackColor;
            timer1.Stop();
            //timer2.Stop();
        }

        private void updateHead()
        {
            DataTable tt = db.Tables["parts"];
            phones_count = tt.Rows.Count;
            object [] t = {fileNameDB, phones_count.ToString()};
            Text = String.Format("Телефоны - [{0}] телефонов в базе: {1}", t);
        }

        private void createDefaultDB()
        {
            db.Clear();
            db.Tables.Clear();

            db.DataSetName = "TELEPHONE DB PARTY";

            DataTable t = db.Tables.Add("parts");

            t.Columns.Add("parent", Type.GetType("System.String")).Caption = "Владелец";
            t.Columns.Add("phone", Type.GetType("System.String")).Caption = "Номер телефона";
            t.Columns.Add("datetime", Type.GetType("System.DateTime"));

            DataColumn c = t.Columns.Add("key", Type.GetType("System.Int32"));
            c.AutoIncrement = true;
            c.AutoIncrementSeed = 0;
            c.AutoIncrementStep = 1;
            c.Unique = true;
            c.ReadOnly = true;
        }

        private void installDataToControlls()
        {
            DataView dv = new DataView(db.Tables["parts"]);
            dataGridView1.DataSource = dv;
            dataGridView1.Columns["parent"].HeaderText = "Владелец";
            dataGridView1.Columns["parent"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["phone"].HeaderText = "Номер телефона";
            dataGridView1.Columns["phone"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["datetime"].HeaderText = "Дата, время";
            dataGridView1.Columns["datetime"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private bool loadFileDB(string fileName_)
        {

            if (File.Exists(fileName_))
            {
                db.ReadXml(fileName_, XmlReadMode.ReadSchema);
                return true;
            }

            return false;
        }

        private void storeDB( string fileName_ )
        {
            db.WriteXml(fileName_, XmlWriteMode.WriteSchema);
            saved = true;
        }

        private int phones_count = 0;

        private string _fileNameDBValue = "";
        private string fileNameDB
        {
            get { return _fileNameDBValue; }
            set 
            { 
                _fileNameDBValue = value;
                //Text = my_programm_name + " - [" + _fileNameDBValue + "]";
                updateHead();
                if ( loadFileDB(_fileNameDBValue) )
                    installDataToControlls();
            }
        }

        private bool _saved_value;
        private bool saved
        {
            get { return _saved_value; }
            set { 
                    _saved_value = value; button_StoreDB.Enabled = !_saved_value;
                    if (_saved_value) 
                    { 
                        button_StoreDB.BackColor = Color.LightGreen;
                        labelDBisSaved.Text = "сохранена";
                        timerStop();
                    }
                    else
                    {
                        button_StoreDB.BackColor = Color.Red; 
                        labelDBisSaved.Text = "НЕ СОХРАНЕНА";
                        timerStart();
                    }
                }
        }

        private DataSet db;

        //*************************************************************************

        private void disableSortingDataGridView()
        {
            foreach (DataGridViewColumn c in dataGridView1.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private string my_path = Application.StartupPath.ToString();
        //private string my_programm_name = "Телефоны";

        private void Form1_Load(object sender, EventArgs e)
        {
            db = new DataSet();

            createDefaultDB();
            installDataToControlls();

            saved = false;

            //Text = my_programm_name;
            updateHead();

            openFileDialog1.InitialDirectory = my_path;

            saveFileDialog1.InitialDirectory = my_path;

            timer1.Enabled = true;
            timer2.Enabled = true;
            timerStop();

            //disableSortingDataGridView();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //saved = false;
        }

        private void button_StoreDB_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
            timerStop();

            saveFileDialog1.FileName = "";

            if (fileNameDB == "")
            {
                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //fileNameDB = Path.GetFileName( saveFileDialog1.FileName );
                    _fileNameDBValue = Path.GetFileName(saveFileDialog1.FileName);
                    storeDB( fileNameDB );
                }
            }
            else
            {
                storeDB(fileNameDB);
            }

            //timer1.Enabled = true;
            timerStart();
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            //saved = false;
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter || e.KeyData == Keys.Tab)
            {
                e.SuppressKeyPress = true;
                textBox1.Focus();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
            timerStop();

            if (!saved && db.Tables["parts"].Rows.Count > 0)
            {
                MessageBox.Show("Не сохранена база. Сохрани!!!");
                return;
            }

            saveFileDialog1.FileName = "";

            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileNameDB = Path.GetFileName( saveFileDialog1.FileName);
                createDefaultDB();
                installDataToControlls();
                storeDB(fileNameDB);
            }

            //timer1.Enabled = true;
            timerStart();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;

            openFileDialog1.FileName = "";

            timerStop();

            if (!saved && db.Tables["parts"].Rows.Count > 0)
            {
                MessageBox.Show("Не сохранена база. Сохрани!!!");
                return;
            }

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                db.Clear();
                fileNameDB = Path.GetFileName( openFileDialog1.FileName);
                updateHead();
                saved = true;
            }

            //timer1.Enabled = true;
            timerStart();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter || e.KeyData == Keys.Tab)
            {
                DataTable t = db.Tables["parts"];

                DataRow r = t.NewRow();
                r["phone"] = textBox1.Text;
                r["parent"] = textBox2.Text;
                r["datetime"] = DateTime.Now.ToString("HH:mm:ss");
                t.Rows.Add(r);

                e.SuppressKeyPress = true;

                textBox2.Focus();
                textBox2.Text = "";
                textBox1.Text = "";
                saved = false;

                updateHead();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
            timerStop();
            printPreviewDialog1.ShowDialog();
            //timer1.Enabled = true;
            timerStart();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
            timerStop();
            printDialog1.ShowDialog();
            //timer1.Enabled = true;
            timerStart();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            DataTable t = db.Tables["parts"];
            printTablePhone.printTable(e, t);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!saved && fileNameDB != "")
            {
                DialogResult dr = MessageBox.Show("Внимание!!!\nИмеются несохраненные изменения.\nСохранить базу на диске?",
                    "Предупреждение!!!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning);
                if( dr == System.Windows.Forms.DialogResult.Yes )
                    storeDB(fileNameDB);

                //e.Cancel = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if( !saved && fileNameDB != "" )
                storeDB(fileNameDB);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private bool f = false;

        private void timer2_Tick(object sender, EventArgs e)
        {
            //if (f)
            //    labelAutoSaveState.Text = "ВКЛ";
            //else
            //    labelAutoSaveState.Text = "   ";
            //f = !f;
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            saved = false;
        }
    }

    public static class printTablePhone
    {

        private static int _current_row_ = 0;
        //Эта функция должна вызываться в событии PrintPage
        // _table_ - печатаемая таблица
        public static void printTable(System.Drawing.Printing.PrintPageEventArgs e, DataTable _table_)
        {

            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Font PrintFont = new Font("Calibri", 3.25f, FontStyle.Bold, GraphicsUnit.Millimeter);
            Pen p = new Pen(Color.Black, 0.35f);
            Pen p1 = new Pen(Color.Black, 0.2f);

            //setup ========
            int cols_cnt = 3;
            int lines_count_in_block = 12;
            int block_on_page = 5;
            float h = 3.75f;// мм
            float w = 46.67f;// мм
            // ==============

            int top = 15;
            int left = 15;

            int current_block = 0;

            int rows_count = _table_.Rows.Count;

            while (current_block < block_on_page)
            {
                for (int rows = 0; rows < lines_count_in_block && (_current_row_ < rows_count); rows++)
                {
                    for (int cols = 0; cols < cols_cnt && (_current_row_ < rows_count); cols++)
                    {
                        float y = rows * h + top;
                        float x = cols * w + left;
                        e.Graphics.DrawRectangle(p, x, y, w, h);
                        DataRow r = _table_.Rows[_current_row_];
                        e.Graphics.DrawString(r["parent"].ToString(), PrintFont, Brushes.Black, new PointF(x, y));
                        e.Graphics.DrawString(r["phone"].ToString(), PrintFont, Brushes.Black, new PointF(x + 33, y));
                        e.Graphics.DrawLine(p1, x + 33, y, x + 33, y + h);
                        _current_row_++;
                    }
                }

                top += (int)(12 * h + 1);
                current_block++;
            }

            e.HasMorePages = _current_row_ < rows_count;
            if (_current_row_ == rows_count) _current_row_ = 0;
        }
    }

}
